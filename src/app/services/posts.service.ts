import { Injectable } from '@angular/core';
import { Firestore, addDoc, collection, collectionData, doc } from '@angular/fire/firestore';
import { getDownloadURL, getStorage, ref, uploadBytes } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor( 
    private afs: Firestore,
    private toastr: ToastrService,
    private router: Router
    ) { }
  
  uploadImage(selectedImgage, postData) {
    const storage = getStorage();
    const filePath = `postIMG/${Date.now()}`
    const storageRef = ref(storage, filePath);

    uploadBytes(storageRef, selectedImgage).then(() => {

      getDownloadURL(storageRef).then(URL => {
        postData.postImgPath = URL
        console.log(postData)

        this.saveData(postData)
      })

    })
  }

  saveData(postData) {
    const collectionInstance = collection(this.afs, 'posts')
    addDoc(collectionInstance, postData).then(docRef => {
      this.toastr.success('Data Insert Successfully ..!')
      this.router.navigate(['/posts'])
    })
  }

  loadData() {
    const collectionInstance = collection(this.afs, 'posts')

    return collectionData(collectionInstance, { idField: 'id' }).pipe(
      map(action => {
        return action.map(a => {
          const data = a
          const id = a['id']

          return { id, data }
        })
      })
    )
  }

  loadOneData(id) {
   return doc(this.afs, `posts/${id}`, id)
  }

}
